%%%%%%%%%%% In the name of GOD %%%%%%%%%%%
% Course: DSP BSC course - Dr. Sheikhzadeh
% demo program by amir hossein rassafi in Feb,2018
clc
clear
a=0.9;
% pole
figure
freqz([1],[1 -a],1024,'whole')
legend("1 pole freq response")
% zero
figure
freqz([1 a],[1],1024,'whole')
legend("1 zero freq response")



