%%%%%%%%%%% In the name of GOD %%%%%%%%%%%
% Course: DSP BSC course - Dr. Sheikhzadeh
% demo program by amir hossein rassafi in Feb,2018
clc
clear
[y,Fs] = audioread('test.mp3');
n = 0:1/Fs:(length(y)-1)/Fs;
noise = zeros(1,length(n));
MAL = 6;
harmonic = [[4000 0.1] ;[7900 0.01]];
 for h=harmonic'
     noise = noise + h(2)*cos(2*pi*h(1)*n);
 end
 %%
 figure
 freqz(ones(1,MAL),[1],1024,Fs)
 legend("moving average DTFT")
 figure
 freqz(noise,[1],1024,Fs)
 legend("noise DTFT")
 figure
 freqz(y,[1],1024,Fs)
 legend("sound DTFT")
 %%
 figure
 freqz(y'+noise,[1],1024,Fs)
 legend("noisy input")
 figure
 freqz(conv(y'+noise,ones(1,MAL)),[1],1024,Fs)
 legend("conv with MA")
 %%
figure
spectrogram(y'+noise,kaiser(1024,5),0,2048,Fs,'yaxis');
p = audioplayer(y'+noise,Fs)
playblocking(p)
%%
figure
spectrogram(conv(y'+noise,ones(1,MAL)),kaiser(1024,5),0,2048,Fs,'yaxis');
p = audioplayer(conv(y'+noise,ones(1,MAL)),Fs)
play(p)
