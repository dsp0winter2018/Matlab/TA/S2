# _MATLAB TA session 2_

## "freqztest.m" file
![image](/uploads/cec998a12b1cbae3010fe725add7a9b8/image.png)


![image](/uploads/a2a4cacbdb93b48fc0044f4d14bd61ec/image.png)


![image](/uploads/369adf29a8fd13c382d25da3a3832b6c/image.png)

## "freqzpole.m" file
![image](/uploads/1345e6da37e0e7de128bbd6c4062eedd/image.png)


## Download gitbash 

### _please download gitbash [here](https://git-scm.com/downloads)_

git clone URL

git pull
